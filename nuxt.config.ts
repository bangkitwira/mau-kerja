import { defineNuxtConfig } from "nuxt";
require("dotenv").config();
// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  app: {
    head: {
      script: [
        {
          src: "assets/js/bootstrap.bundle.min.js",
        },
      ],
      link: [
        {
          rel: "stylesheet",
          href: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css",
        },
      ],
    },
  },
  css: ["@/assets/css/style.css", "@/assets/css/bootstrap.min.css"],

  components: {
    global: true,
    dirs: ["~/components"],
  },
  env: {
    API_URL: process.env.API_URL
  },
  modules: ["@nuxtjs/tailwindcss", "@nuxtjs/dotenv"],
});
