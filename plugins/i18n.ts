import { createI18n } from 'vue-i18n'

import en from '../locales/en.json'
import id from '../locales/id.json'

const i18n = createI18n({
	legacy: false,
	globalInjection: true,
	locale: 'en',
	fallbackLocale: 'en',
	messages: {
		en,
		id,
	},
})

export default defineNuxtPlugin((nuxtApp) => {
	nuxtApp.vueApp.use(i18n)
})