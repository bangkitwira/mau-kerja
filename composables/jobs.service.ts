import { ResponseData } from "@/types/Response";

const useJobService = () => {
  const getJobs = async (params = null) => {
    const { data: data } = await useFetch<ResponseData>(
      `/api/jobs${params && params.substring(1)}`
    );
    if (data) {
      return data.value.data;
    }
  };

  return {
    getJobs
  };
};

export default useJobService;
