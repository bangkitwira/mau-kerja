# Mau Kerja Nuxt Project

Test project to create mau kerja website using Nuxt, bootstrap and tailwind as ui components and css helper, vue-i18n as internalization.

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```
# env
copy .env-example and create a new env files and fill the API_URL 

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev or yarn run dev
```

## Production

Build the application for production:

```bash
npm run build or yarn run build
```

Locally preview production build:

```bash
npm run preview or yarn run preview
```
