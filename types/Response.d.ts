import { Job } from "@/types/Job";

export interface ResponseData {
  currentPage: number;
  data: Job[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: string;
  prev_page_url: string | null;
  to: number;
  total: number;
}
