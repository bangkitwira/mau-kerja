import useJobService from "@/composables/jobs.service";
import { createStore } from "vuex";

const { getJobs } = useJobService();
export const store = createStore({
  state() {
    return {
      jobs: [],
      isLoading: false,
    };
  },
  actions: {
    async fetchJobs({ commit, state }) {
      // make requests
      state.isLoading = true;
      try {
        const response = await getJobs();
        commit("SET_JOBS", response);
        state.isLoading = false;
      } catch {
        state.isLoading = false;
      }
    },
    async fetchFilteredJobs({ commit, state }, payload) {
      // make requests
      state.isLoading = true;
      try {
        const response = await getJobs(payload);
        commit("SET_JOBS", response);
        state.isLoading = false;
      } catch {
        state.isLoading = false;
      }
    },
  },
  mutations: {
    SET_JOBS(state, jobs) {
      state.jobs = jobs;
    },
  },
});
