export default defineEventHandler(async (event) => {
  const query = useQuery(event);
  let url = new URL(process.env.API_URL);
  if (query) {
    for (let q in query) {
      url.searchParams.append(q, query[q]);
    }
  }
  return await fetch(url)
    .then((response) => response.json())
    .catch((error) => console.log(error));
});
